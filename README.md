# Unicorn Burrito
This project is an attempt at building a neural network, training, and tweaking it to take user input and generate an appropriate sounding response.

## Dependencies
* Deeplearning4j (dl4j) - heavy dependency
* Stanford NLP (SNLP) - light dependency, could be refactored out
    * Stanford's NLP library was integrated, but later became largely disabled. It could potentially be refactored out, and a different tokenization system and entity recognition system could be used instead.

[Click here to download dependencies 3GB](https://drive.google.com/open?id=1blAlGyxBk9JTtVXEBcRT5U25rlmGadys) Extract this to the repository's root directory, and you should have a root level `lib` directory with the packages necessary inside it.

## Building
* It is assumed the user is familiar with importing projects into their local environment.
* Import the ChatBot directory as the project source root, or the repo root directory.
    * The important thing to note is that it is better to exclude the example code directory
* Set the build dependencies as being all of the packages in `lib` except for the "mvn" dl4j directory. This was included solely for the documentation of dl4j it can provide, but is incompatible with building unfortunately.

## Running this program
Important notes about running this program:

* Command line launch options:
    * `-load <file>` loads a serialized `NeuralNetwork` save file & a serialized `NetworkDictionary`
    * `-train <file>` builds a `NetworkDictionary` then trains a `NeuralNetwork` based on the `NetworkDictionary` vocabulary size
    * `-retrain <file>` loads a serialized `NeuralNetwork` save file & a serialized `NetworkDictionary`, and then begins training the `NeuralNetwork`
* It loads a Data Set (33MB/304k+ lines of dialogue)
* Build a `Corpus`, to later iterate, with the loaded data
* Train the neural network
    * This can take hours. The master branch is set to do a shallow training. On the CPU
    * `CUTOFF` and line 134 in Main.java can be used to modify how deeply the model is trained
* Provide input to command line prompt, and await a response, repeat indefinitely

## ChatBot Classes
###### Main.java
Implements main method, performs program initializations, and runs core program loop.

* Parses command line arguments to (accepted parameters `-load <file>` or `-train <file>` or `-retrain <file>`)
* Looks for a dictionary file to load
    * Builds dictionary if none is found
* Implements chat loop
    * Queries user input
    * Generates bot response

Parses command line arguments to ascertain the files to load/save serialized objects from. Specifically we serialize our `NetworkDictionary` & `NeuralNetwork` as they each take a considerable amount of time to generate. So they are saved to disk as relatively large files.

###### RNN/NeuralNetwork.java
Implements multi-layered computational graph. Intended for training with a data set to later make predictions based on input data.

* Implements `serial_train(Corpus,NetworkDictionary)`
* Implements `para_train(Corpus,NetworkDictionary)`
* Implements `batch_train(Corpus,NetworkDictionary)` - non functional
* Implements `serialSave(File)`
* Implements `serialLoad(File)`

###### RNN/NetProperties.java
POD structure for encapsulating general properties needed for building the NeuralNetwork. Such as the learning rate, or back propogation type.

###### RNN/LayerProperties.java
POD structure for encapsulating layer properties. Such as activation functions, and the number of inputs and outputs per layer.

###### Corpus/Corpus.java
Implements Corpus data set loading and pre-processing. Builds an iterable structure with the data set content.

* Implements a collection of `ConvoGroup` objects
* Implements `loadCorpus()`
    * Implements `loadConversations()` helper method to load dialogue -> conversation mappings
        * Builds `Conversation` objects (stored in `ConvoGroup` objects)
    * Implements `loadDialogue()` helper method to load dialogue lines into appropriately mapped conversations
        * Loads Dialogue Lines into corresponding `Conversation` objects
* Implements `buildSentences()`

###### Corpus/Convo/ConvoGroup.java
Collection of `Conversation` objects.

###### Corpus/Convo/Conversation.java
Collection of `ConvoLine` objects.

###### Corpus/Convo/ConvoLine.java
POD structure representing a single line of dialogue, and neighbouring lines. Think double linked list, but as a POD.
Contains:

* raw string
* `Sentence` object
* speaker id
* next `ConvoLine`
* prev `ConvoLine`

###### Corpus/Convo/ConvoIterator.java
Provides an iterable interface for traversing Conversational data.

* Implements a `MultiDataSetIterator` interface
    * Implements `hasNext()`
    * Implements `next()`
    * Implements `next(int num)` - incomplete functionality
    * Implements `reset()`
* Additionally implements helper methods
    * Implements `hasNextConvo()`
    * Implements `hasNextGroup()`
    * Implements `proceedToValidNext()`
    * Implements `proceedToValidNextConvo()`
    * Implements `serialNext()` - substitute for `next(int num)`

###### Dictionary/NetworkDictionary.java
Provides an interface for mapping `Sentence` data to encodings representing that data. 
(eg. the token "she" => '162.0')

* Implements an encoding/decoding style interface
    * Implements `mapSentence(Sentence)`
    * Implements `encodePrompt(Sentence)` - unused for reasons related to training the RNN
    * Implements `encodeResponse(Sentence)` - used for prompts & responses to simplify training the RNN
    * Implements `decodeResponse(List<Double>)`
    * Implements `decodeWord(double)`
* Implements `serialSave(File)`
* Implements `serialLoad(File)`
