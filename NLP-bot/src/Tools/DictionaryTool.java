package Tools;

import ChatBot.Dictionary.NetworkDictionary;
import edu.stanford.nlp.simple.Sentence;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DictionaryTool {
    private static File vocab_file = null;
    private static File vocab_lookup_file = null;
    private static File merged_dictionary_file = null;

    public static void  main(String[] args) {
        try {
            final Path MODELSAVE_PATH = FileSystems.getDefault().getPath("Model");
            vocab_file = new File("Vocabulary.test");
            vocab_lookup_file = new File("Vocabulary_lookup.test");
            merged_dictionary_file = new File(MODELSAVE_PATH + "\\merged_dictionary.dat");
            fix_dictionary_files();
            test_merged_dictionary();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void fix_dictionary_files() throws Exception {
        System.out.println("fixing dictionary files");
        if(!vocab_file.exists()){
            throw new Exception("Vocabulary file doesn't exist");
        }
        if (!vocab_lookup_file.exists()) {
            vocab_lookup_file.createNewFile();
        }
        System.out.println("Building vocabulary lookup");
        build_rev_dict(vocab_file,vocab_lookup_file);

        ArrayList<File> dicts = new ArrayList<>();
        dicts.add(vocab_file);
        dicts.add(vocab_lookup_file);
        dicts.add(new File("Ner.test"));
        dicts.add(new File("Lemma.test"));
        dicts.add(new File("Pos.test"));

        System.out.println("merging dictionary files..");
        if (!merged_dictionary_file.exists()) {
            merged_dictionary_file.getParentFile().mkdirs();
            merged_dictionary_file.createNewFile();
        }
        merge(dicts,merged_dictionary_file);
    }

    private static void build_rev_dict(File inFile, File outFile) throws Exception {
        if (inFile.exists()) {
            if (!outFile.exists()) {
                outFile.getParentFile().mkdirs();
                outFile.createNewFile();
            } else {
                outFile.delete();
            }
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(inFile));
            Map<String,Double> regDict = (Map)in.readObject();
            in.close();

            Map<Double,String> revDict = new HashMap<>();
            for(Map.Entry<String,Double> entry : regDict.entrySet()){
                revDict.put(entry.getValue(),entry.getKey());
            }

            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(outFile));
            out.writeObject(revDict);
        } else {
            throw new Exception("File for regularly dictionary does not exist");
        }
    }

    private static void merge(ArrayList<File> dictPaths, File outFile) throws Exception {
        if(!outFile.exists()) {
            outFile.getParentFile().mkdirs();
            outFile.createNewFile();
        } else {
            outFile.delete();
        }
        //where data will be stored
        ObjectOutputStream dictFile = new ObjectOutputStream( new FileOutputStream(outFile));

        for(File f : dictPaths){
            if(f.exists()) {
                ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));
                dictFile.writeObject(in.readObject());
                in.close();
            }
        }

        dictFile.close();
    }

    private static void test_merged_dictionary(){
        NetworkDictionary dictionary = new NetworkDictionary();
        System.out.println("loading merge file..");
        dictionary.serialLoad(merged_dictionary_file);
        String text = "this is a quick test";
        System.out.println("Encoding sentence: " + text);
        List<Double> encoding = dictionary.encodeResponse(new Sentence(text));
        System.out.println("Decoding..");
        System.out.println(dictionary.decodeResponse(encoding));
        System.out.println("Done. Successful if strings read the same.");
    }

}
