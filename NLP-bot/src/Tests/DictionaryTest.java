package Tests;
import ChatBot.Dictionary.NetworkDictionary;
import edu.stanford.nlp.simple.Sentence;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.*;

public class DictionaryTest {
    private static String text = "Hello, my NAME is is Emily and I Am tEsting thE Libraries.";
    private static NetworkDictionary dict = new NetworkDictionary();

    public static void main(String[] args){
        //orig_test_fn();
        save_load();
    }

    private static void orig_test_fn(){
        NetworkDictionary newDict = new NetworkDictionary();
        Sentence sentence = new Sentence(text);
        newDict.mapSentence(sentence);
        Map<String, Double> testMap = (newDict.getVocab());
        System.out.println(testMap.keySet().toString());
        List keys = new ArrayList(testMap.keySet());

        // Prints the vocab library and values to test
        for (int i = 0; i < keys.size(); i++) {
            System.out.print(testMap.get(keys.get(i)));
            System.out.print(", ");
        }
        // Prints the lemma library to test
        Map<String, Double> lemmaTest = (newDict.getLemma());
        System.out.println(lemmaTest.keySet().toString());

        Map<String, Double> nerTest = (newDict.getNER());
        System.out.println(nerTest.keySet().toString());
        List nerKeys = new ArrayList(nerTest.keySet());

        for (int i = 0; i < nerKeys.size(); i ++){
            System.out.print(nerTest.get(nerKeys.get(i)));
            System.out.print(", ");
        }
        System.out.println(newDict.getNerRelations().toString());

        Map<String, Double> posTest = (newDict.getPOS());
        System.out.println(posTest.keySet().toString());
        // Prints out the frequencies to assure it is counting properly
        Map<String, Integer> testFreq = (newDict.getFrequency());
        System.out.println(testFreq.keySet().toString());
        List freqs = new ArrayList(testFreq.keySet());
        for (int i = 0; i < freqs.size(); i++){
            System.out.print(freqs.get(i));
            System.out.print(": ");
            System.out.print(testFreq.get(freqs.get(i)));
            System.out.println(", ");
        }
      /*  // Save created data
        newDict.serialSave();
        // Delete vocabulary data
        newDict.clear();
        // Load saved vocabulary data
        newDict.serialLoad();
        Map<String, Double> testMapLoad = (newDict.getVocab());
        System.out.println(testMapLoad.keySet().toString());
        List keysLoaded = new ArrayList(testMapLoad.keySet());

        // Prints the vocab library keys to ensure data loaded
        for (int i = 0; i < keysLoaded.size(); i++) {
            System.out.print(testMapLoad.get(keys.get(i)));
            System.out.print(", ");
        }



*/

        //Test the encoding of the words
        List<List<Double>> encodingTest = new ArrayList<>(newDict.encodePrompt(sentence));
        for (int i = 0; i < encodingTest.size(); i ++) {
            System.out.println(encodingTest.get(i).toString());
        }
        // Test the decoding of the words
        List<Double> decodingTest = new ArrayList<>(newDict.encodeResponse(sentence));
        System.out.println(decodingTest);
        System.out.println(newDict.decodeWord(decodingTest.get(0)));

        //Test the decoding of a sentence.
        System.out.println(newDict.decodeResponse(decodingTest));

        System.out.println(newDict.vocabularySize());
        System.out.println(newDict.encodingSize());
    }

    public static void save_load(){
        try {
            final Path MODELSAVE_PATH = FileSystems.getDefault().getPath("Model");
            File dictionary_file = new File(MODELSAVE_PATH + "\\uber-dict-test.dat");
            if (!dictionary_file.exists()) {
                dictionary_file.getParentFile().mkdirs();
                dictionary_file.createNewFile();
            }
            String local_text = "This IS A Frigckning test Of Saving anD loading, and sanitization.";
            Sentence sent = new Sentence(local_text);
            System.out.println(local_text);
            System.out.println(sent.words().toString());
            dict.mapSentence(sent);
            List<Double> encoded = dict.encodeResponse(sent);
            dict.serialSave(dictionary_file);
            dict.clear();
            dict.serialLoad(dictionary_file);
            System.out.println(dict.decodeResponse(encoded));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
