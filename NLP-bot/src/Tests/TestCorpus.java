package Tests;

import ChatBot.Corpus.Convo.Conversation;
import ChatBot.Corpus.Convo.ConvoGroup;
import ChatBot.Corpus.Convo.ConvoLine;
import ChatBot.Corpus.Corpus;

import java.util.Map;

public class TestCorpus {
    public static void main(String[] args) {
        try {
            Corpus corp = new Corpus();
            System.out.println("Loading corpus..");
            corp.loadCorpus();
            System.out.println("Corpus loaded.");

            for(Map.Entry<Integer,ConvoGroup> entry : corp.getDataSet().entrySet()){
                System.out.println("convo group: " + entry.getValue());
                for(Conversation convo : entry.getValue().getConversations()){
                    System.out.println("\nconvo: " + convo);
                    int line_num = 0;
                    for(ConvoLine line : convo.getConversation()){
                        System.out.println("line number: " + convo.getLineNumber(line_num++) + " -- " + line.line + " -- speaker: " + line.speaker_id);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
