package Tests.LibraryTesting;

import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.semgraph.*;
import edu.stanford.nlp.simple.*;
import java.util.*;

public class TestStanfordNLP {
    public static String text = "Joe Smith was born in California. " +
            "In 2017, he went to Paris, France in the summer. " +
            "His flight left at 3:00pm on July 10th, 2017. " +
            "After eating some escargot for the first time, Joe said, \"That was delicious!\" " +
            "He sent a postcard to his sister Jane Smith. " +
            "After hearing about Joe's trip, Jane decided she might go to France one day.";

    public static String text2 = "Josh was impressed at how complex the resumé was.";
    public static String text3 = "But it won't be. Not as long as you insist on living in the past!";
    public static String text4 = "Oh, can't, I'm afraid. Matinée of Les Miserables. Listen, I've really got to go. I'll-Oh...Christ...I'll call you.";

    public static void main(String[] args){
        InitSimpleAPI();
        SimpleAPI();
    }

    public static void InitSimpleAPI(){
        System.out.println("Initializing Simple API...");
        Sentence dummy = new Sentence("Hello world.");
        System.out.println("NER/Classifier...");
        dummy.nerTags();
        System.out.println("Sentiment parsing...");
        dummy.sentiment();

    }

    public static void SimpleAPI(){
        System.out.println("Starting simple.");
        System.out.println(text4);
        Sentence secondRun = new Sentence(text4);
        System.out.println("sentiment: " + secondRun.sentiment().toString());
        System.out.println("tokens: " + secondRun.words().toString());
        System.out.println("lemmas: " + secondRun.lemmas().toString());
        System.out.println("PoS tags: " + secondRun.posTags().toString());
        System.out.println("NER tags: " + secondRun.nerTags().toString());
        System.out.println("'mentions': " + secondRun.mentions().toString());
        System.out.println("dependencies:\n\n");

        SemanticGraphFactory.Mode mode;
        mode = SemanticGraphFactory.Mode.BASIC;
        SemanticGraph dep = secondRun.dependencyGraph(mode);
        int i = 0;
        for(IndexedWord word : dep.vertexSet()){
            System.out.println(secondRun.tokens().get(i++).originalText());
            System.out.println("parents: " + dep.getParents(word).toString());
            System.out.println("children: " + dep.getChildren(word).toString());
            System.out.print("children raw words: [");
            Iterator<IndexedWord> iter = dep.getChildren(word).iterator();
            while(iter.hasNext()){
                System.out.print(iter.next().word() + ", ");
            }
            System.out.println("]");
            System.out.println("child pairs: " + dep.childPairs(word).toString());
            System.out.println();
        }

        System.out.println("done.");
    }

}