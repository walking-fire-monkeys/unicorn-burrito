package ChatBot.Corpus;

import ChatBot.Corpus.Convo.Conversation;
import ChatBot.Corpus.Convo.ConvoGroup;
import ChatBot.Corpus.Convo.ConvoIterator;
import ChatBot.Corpus.Convo.ConvoLine;
import edu.stanford.nlp.simple.Sentence;

import java.io.LineNumberReader;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Corpus
 * Loads conversational data, movie dialogue specifically.
 *
 * Note: It may be possible to find an memory size optimization in storing encodings
 * into the ConvoLines. Both prompt and response encodings would be necessary, unless
 * we revise the prompt and response encodings to be the same
 * */
public class Corpus {
    private int num_groups = 0;
    private int num_conversations = 0;
    private int num_lines = 0;
    private HashMap<Integer,ConvoGroup> data_set = new HashMap<>();
    private File conversations = null;
    private File dialogue = null;

    public HashMap<Integer,ConvoGroup> getDataSet(){
        return data_set;
    }

    public void clearDataSet(){
        num_groups = 0;
        num_conversations = 0;
        num_lines = 0;
        data_set.clear();
    }

    public void buildSentences(){
        int counter = 0;
        for(ConvoGroup group : data_set.values()){
            for(Conversation convo : group.getConversations()){
                for(ConvoLine convo_line : convo.getConversation()){
                    if(++counter % 10000 == 0){
                        System.out.println("sentences built: " + counter);
                    }
                    convo_line.sent = new Sentence(convo_line.line);
                }
            }
        }
    }

    public void loadCorpus() throws Exception {
        final String CONVERSATIONS_FILENAME = "\\movie_conversations.txt";
        final String DIALOGUE_FILENAME = "\\movie_lines.txt";
        final Path DATA_PATH = FileSystems.getDefault().getPath("DataSets");
        conversations = new File(DATA_PATH + CONVERSATIONS_FILENAME);
        dialogue = new File(DATA_PATH + DIALOGUE_FILENAME);
        if(conversations.exists() && dialogue.exists()){
            loadConversations();
            loadDialogue();
        } else {
            String msg = "File missing.\n";
            msg += conversations.getAbsolutePath() + " exists: " + conversations.exists() + "\n";
            msg += dialogue.getAbsolutePath() + " exists: " + dialogue.exists();
            throw new Exception(msg);
        }
    }

    protected void loadConversations(){
        System.out.println("Conversations File is " + conversations.length() + " bytes.");
        try {
            BufferedReader inStream = new BufferedReader(new FileReader(conversations));
            String line = "";
            while((line = inStream.readLine()) != null){
                //note: we have an extra, empty, token after splitting. otherwise we're fine
                String[] tokens = line.split("[^-0-9]+");

                List<Integer> line_nums = new ArrayList<>(tokens.length-4);
                for(int i = 4; i < tokens.length; ++i){
                    line_nums.add(Integer.parseInt(tokens[i]));
                }

                int movie_id = Integer.parseInt(tokens[3]);
                if(!data_set.containsKey(movie_id)){
                    data_set.put(movie_id,new ConvoGroup());
                    ++num_groups;
                }
                data_set.get(movie_id).addConvo(new Conversation(line_nums));
                ++num_conversations;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void loadDialogue(){
        System.out.println("Dialogue File is " + dialogue.length() + " bytes.");
        String line = "";
        try {
            LineNumberReader counter = new LineNumberReader(new FileReader(dialogue));
            counter.skip(Long.MAX_VALUE);
            System.out.println("lines to load: " + counter.getLineNumber());
            counter.close();

            BufferedReader inStream = new BufferedReader(new FileReader(dialogue));
            while((line = inStream.readLine()) != null){
                //note: we have an extra, empty, token after splitting (on both splits)
                String[] number_tokens = line.split("[^-0-9]+");
                String[] simple_array = line.split(".*(\\+\\+\\+ )"); //the second element should be the convo_line string
                String sentRead = "";
                if(simple_array.length == 2){
                    sentRead = simple_array[1].trim();
                } else {
                    sentRead = "uhh";
                }
                if(sentRead.length() == 0){
                    sentRead = "uhh";
                }

                int line_num = Integer.parseInt(number_tokens[1]);
                int speaker_id = Integer.parseInt(number_tokens[2]);
                int movie_id = Integer.parseInt(number_tokens[3]);
                ConvoLine convo_line = new ConvoLine(speaker_id,sentRead);
                //no null checks below, because the exceptions it WILL throw are enough
                data_set.get(movie_id).findConversation(line_num).add(convo_line,line_num);
                ++num_lines;
            }
        } catch (Exception e) {
            System.out.println(line);
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return String.format("Corpus [ 'groups': %d, 'conversations': %d, 'lines': %d ]",num_groups,num_conversations,num_lines);
    }
}
