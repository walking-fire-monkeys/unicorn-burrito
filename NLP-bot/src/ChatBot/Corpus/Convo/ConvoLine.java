package ChatBot.Corpus.Convo;

import edu.stanford.nlp.simple.Sentence;

public class ConvoLine {
    public int speaker_id = 0;
    public String line = "";
    public Sentence sent = null;
    public ConvoLine next = null;
    public ConvoLine prev = null;

    public ConvoLine ( int speaker_id, String line ) {
        this.speaker_id = speaker_id;
        this.line = line;
    }
}
