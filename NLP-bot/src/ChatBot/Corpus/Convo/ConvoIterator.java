package ChatBot.Corpus.Convo;

import ChatBot.Corpus.Corpus;
import ChatBot.Dictionary.NetworkDictionary;
import ChatBot.Main;
import edu.stanford.nlp.simple.Sentence;
import org.apache.commons.lang3.ArrayUtils;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.MultiDataSet;
import org.nd4j.linalg.dataset.api.MultiDataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;

import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Iterates through conversations
 * Groups dialogue into pairs of: [prompt,response]
 * Each prompt and response constitutes a single example for training a neural network.
 * */
public class ConvoIterator implements MultiDataSetIterator {
    //deferred-todo ensure thread safety
    private boolean are_we_batching = false;
    private int counter = 0;
    private int cutoff = 900000;
    private ReentrantLock batchLock = new ReentrantLock();
    private NetworkDictionary dictionary;
    private Map<Integer,ConvoGroup> convo_groups;
    private Iterator<ConvoGroup> current_group;
    private Iterator<Conversation> current_convo;
    private Iterator<ConvoLine> current_line;

    private Iterator<ConvoGroup> current_group_copy;
    private Iterator<Conversation> current_convo_copy;
    private Iterator<ConvoLine> next_line;

    public ConvoIterator(Corpus dialogue, NetworkDictionary dict) {
        convo_groups = dialogue.getDataSet();
        dictionary = dict;
        initializeIterators();
        are_we_batching = false;
    }

    public ConvoIterator(Corpus dialogue, NetworkDictionary dict, int cutoff){
        convo_groups = dialogue.getDataSet();
        dictionary = dict;
        initializeIterators();
        are_we_batching = false;
        this.cutoff = cutoff;
    }

    protected void initializeIterators(){
        current_group = null;
        current_convo = null;
        current_line = null;
        next_line = null;
        if(convo_groups != null) {
            current_group = convo_groups.values().iterator();
            current_group_copy = convo_groups.values().iterator();
            if (current_group.hasNext()) {
                current_convo = current_group.next().getConversations().iterator();
                current_convo_copy = current_group_copy.next().getConversations().iterator();
                if (current_convo.hasNext()) {
                    current_line = current_convo.next().getConversation().iterator();
                    next_line = current_convo_copy.next().getConversation().iterator();
                    next_line.next();
                }
            }
        }
    }

    /*Resets the iterator back to the beginning*/
    @Override
    public void reset() {
        initializeIterators();
        are_we_batching = false;
    }

    /*recursively finds the next valid example in the data structure*/
    protected void proceedToValidNext(){
        if(!next_line.hasNext()){
            if(current_convo.hasNext()){
                current_line = current_convo.next().getConversation().iterator();
                next_line = current_convo_copy.next().getConversation().iterator();
                next_line.next();
                if(!next_line.hasNext()) {
                    proceedToValidNext();
                }
            } else if(current_group.hasNext()) {
                current_convo = current_group.next().getConversations().iterator();
                current_convo_copy = current_group_copy.next().getConversations().iterator();
                if (current_convo.hasNext()) {
                    current_line = current_convo.next().getConversation().iterator();
                    next_line = current_convo_copy.next().getConversation().iterator();
                    next_line.next();
                    if (!next_line.hasNext()) {
                        proceedToValidNext();
                    }
                }
            }
            //no more data to iterate through
        }
    }

    /*recursively finds the next valid example in the data structure*/
    protected void proceedToValidNextConvo(){
        if(!current_convo.hasNext()){
            if(current_group.hasNext()){
                batchLock.lock();
                current_convo = current_group.next().getConversations().iterator();
                batchLock.unlock();
            }
        }
    }

    /*checks the iteration structure, recursively, for a next example*/
    @Override
    public boolean hasNext() {
        if (counter >= cutoff) {
            return false;
        } else if(are_we_batching) {
            return hasNextConvo();
        } else if(next_line != null) {
            if(next_line.hasNext()) {
                return true;
            }
            proceedToValidNext();
            return next_line.hasNext();
        }
        return false;
    }

    public boolean hasNextConvo(){
        if(!current_convo.hasNext()) {
            proceedToValidNextConvo();
            return current_convo.hasNext();
        }
        return true;
    }

    public boolean hasNextGroup(){
        return current_group.hasNext();
    }

    /*retrieves one example and return it in a `MultiDataSet`*/
    @Override
    public MultiDataSet next() {
        return serialNext();
    }

    public MultiDataSet serialNext(){
        //killed it!!!
        are_we_batching = false;
        ConvoLine prompt = current_line.next();
        ConvoLine response = next_line.next();
        if(prompt.sent == null){
            prompt.sent = new Sentence(prompt.line);
        }
        if(response.sent == null){
            response.sent = new Sentence(response.line);
        }
        List<Double> enc_prompt = dictionary.encodeResponse(prompt.sent);
        List<Double> enc_response = dictionary.encodeResponse(response.sent);
        Collections.reverse(enc_prompt);
        enc_response.add(Main.EOS);

        final int row_size = prompt.line.length() > response.line.length() ? prompt.line.length() : response.line.length();
        final int vocab_size = dictionary.vocabularySize();

        //inputs
        INDArray input = Nd4j.zeros(1, 1, row_size);
        INDArray decode = Nd4j.zeros(1, vocab_size, row_size);

        //output prediction
        INDArray prediction = Nd4j.zeros(1, vocab_size, row_size);

        //masks - what are they good for? dunno yet
        INDArray input_mask = Nd4j.zeros(1, row_size);
        INDArray pred_decode_mask = Nd4j.zeros(1, row_size);

        /**
         * Matrix format
         *  - So we set the column width (ie. row length) to the dictionary size.
         *  - We also set the row height (ie. column length) to the sentence size.
         *
         * Columns:   dictionary word (ie. element 381 is the word mapped to 381 in the dictionary)
         * Rows:      token position (ie. element 0 is the first token of the sentence)
         * Accessing: matrix[column][row]
         *
         * "The fox are red red"
         *   The | fox | are | red
         * -------------------------
         * |  1  |  0  |  0  |  0  |
         * |  0  |  1  |  0  |  0  |
         * |  0  |  0  |  1  |  0  |
         * |  0  |  0  |  0  |  1  |
         * |  0  |  0  |  0  |  1  |
         * -------------------------
         * */

        double predOneHot[][] = new double[vocab_size][enc_response.size()];
        double decodeOneHot[][] = new double[vocab_size][enc_response.size()];
        decodeOneHot[Main.GO.intValue()][0] = 1; //enable <go> bit (one-hot bb)
        int prediction_token_i = 0; //indicates which token is being predicted (ie. the row)
        //building the matrices, formatted as above
        for (Double pred : enc_response) {
            predOneHot[pred.intValue()][prediction_token_i] = 1;
            if (prediction_token_i < enc_response.size() - 1) {
                decodeOneHot[pred.intValue()][prediction_token_i + 1] = 1;
            }
            ++prediction_token_i;
        }

        INDArrayIndex zero = NDArrayIndex.point(0);
        INDArrayIndex prompt_size = NDArrayIndex.interval(0, enc_prompt.size());
        INDArrayIndex response_size = NDArrayIndex.interval(0, enc_response.size());
        INDArrayIndex encoding_size = NDArrayIndex.interval(0, vocab_size);

        //inputs                            // Nd4j.create() - Creates a row vector with data given
        input.put(new INDArrayIndex[]{zero, zero, prompt_size},
                Nd4j.create(ArrayUtils.toPrimitive(enc_prompt.toArray(new Double[0]))));
        decode.put(new INDArrayIndex[]{zero, encoding_size, response_size}, Nd4j.create(decodeOneHot));

        //output prediction
        prediction.put(new INDArrayIndex[]{zero, encoding_size, response_size}, Nd4j.create(predOneHot));

        //masks - what are they good for? dunno yet
        input_mask.put(new INDArrayIndex[]{zero, prompt_size}, Nd4j.ones(enc_prompt.size()));
        pred_decode_mask.put(new INDArrayIndex[]{zero, response_size}, Nd4j.ones(enc_response.size()));
        System.out.println("examples prepared: " + ++counter + " -- thread id: " + Thread.currentThread().getId());

        return new org.nd4j.linalg.dataset.MultiDataSet(
                new INDArray[]{input, decode},
                new INDArray[]{prediction},
                new INDArray[]{input_mask, pred_decode_mask},
                new INDArray[]{pred_decode_mask});
    }

    /*retrieves a specified number of examples and returns them in a `MultiDataSet`*/
    @Override
    public MultiDataSet next(int num_examples) {
        //killed it!!!
        batchLock.lock();
        are_we_batching = true;
        batchLock.unlock();
        final int vocab_size = dictionary.vocabularySize();
        int example_counter = 0;
        int row_size = 100; //assuming a max size for prompt/response lengths
        try {
            /**
             * Batch is a MultiDataSet
             * it contains planes of examples
             * need to prepare the shape of the arrays that go into the multi data set first
             * */

            batchLock.lock();
            Conversation c = current_convo.next();
            int batchSize = c.size();
            batchLock.unlock();
            System.out.println("batch size: " + batchSize);

            //inputs
            INDArray input = Nd4j.zeros(batchSize, 1, row_size);
            INDArray decode = Nd4j.zeros(batchSize, vocab_size, row_size);

            //output prediction
            INDArray prediction = Nd4j.zeros(batchSize, vocab_size, row_size);

            //masks - what are they good for? dunno yet
            INDArray input_mask = Nd4j.zeros(batchSize, row_size);
            INDArray pred_decode_mask = Nd4j.zeros(batchSize, row_size);

            //create example pairs ie. create plane
            for (ConvoLine prompt : c.getConversation()) {
                ConvoLine response = prompt.next;
                if(response == null){
                    break;
                }
                List<Double> enc_prompt = dictionary.encodeResponse(prompt.sent);
                List<Double> enc_response = dictionary.encodeResponse(response.sent);
                Collections.reverse(enc_prompt);
                enc_response.add(Main.EOS);

                /**
                 * Matrix format
                 *  - So we set the column width (ie. row length) to the dictionary size.
                 *  - We also set the row height (ie. column length) to the sentence size.
                 *
                 * Columns:   dictionary word (ie. element 381 is the word mapped to 381 in the dictionary)
                 * Rows:      token position (ie. element 0 is the first token of the sentence)
                 * Accessing: matrix[column][row]
                 *
                 * "The fox are red red"
                 *   The | fox | are | red
                 * -------------------------
                 * |  1  |  0  |  0  |  0  |
                 * |  0  |  1  |  0  |  0  |
                 * |  0  |  0  |  1  |  0  |
                 * |  0  |  0  |  0  |  1  |
                 * |  0  |  0  |  0  |  1  |
                 * -------------------------
                 * */

                double predOneHot[][] = new double[vocab_size][enc_response.size()];
                double decodeOneHot[][] = new double[vocab_size][enc_response.size()];
                decodeOneHot[Main.GO.intValue()][0] = 1; // <go> token
                int prediction_token_i = 0; //indicates which token is being predicted (ie. the row)
                //building the matrices, formatted as above
                for (Double pred : enc_response) {
                    predOneHot[pred.intValue()][prediction_token_i] = 1;
                    if (prediction_token_i < enc_response.size() - 1) {
                        decodeOneHot[pred.intValue()][prediction_token_i + 1] = 1;
                    }
                    ++prediction_token_i;
                }

                INDArrayIndex zero = NDArrayIndex.point(0);
                INDArrayIndex batch_plane = NDArrayIndex.point(++example_counter);
                INDArrayIndex prompt_size = NDArrayIndex.interval(0, enc_prompt.size());
                INDArrayIndex response_size = NDArrayIndex.interval(0, enc_response.size());
                INDArrayIndex encoding_size = NDArrayIndex.interval(0, vocab_size);

                //inputs                            // Nd4j.create() - Creates a row vector with data given
                input.put(new INDArrayIndex[]{batch_plane, zero, prompt_size},
                        Nd4j.create(ArrayUtils.toPrimitive(enc_prompt.toArray(new Double[0]))));
                decode.put(new INDArrayIndex[]{batch_plane, encoding_size, response_size}, Nd4j.create(decodeOneHot));

                //output prediction
                prediction.put(new INDArrayIndex[]{batch_plane, encoding_size, response_size}, Nd4j.create(predOneHot));

                //masks - what are they good for? dunno yet
                input_mask.put(new INDArrayIndex[]{batch_plane, prompt_size}, Nd4j.ones(enc_prompt.size()));
                pred_decode_mask.put(new INDArrayIndex[]{batch_plane, response_size}, Nd4j.ones(enc_response.size()));

                batchLock.lock();
                if(++counter % 100 == 0) {
                    System.out.println("examples prepared: " + counter + " -- thread id: " + Thread.currentThread().getId());
                }
                batchLock.unlock();
            }
            return new org.nd4j.linalg.dataset.MultiDataSet(
                    new INDArray[]{input, decode},
                    new INDArray[]{prediction},
                    new INDArray[]{input_mask, pred_decode_mask},
                    new INDArray[]{pred_decode_mask});
        }
        catch (Exception e ) {
            batchLock.unlock();
            e.printStackTrace();
        }
        return null;
    }

    //deferred-todo enable once thread safety is ensured, and asynchronous can verifiably be supported
    @Override
    public boolean asyncSupported() {
        return false;
    }

    /**
     * Code below is unimportant
     * */


    //indicates whether dl4j is allowed to call reset. (or something like that)
    @Override
    public boolean resetSupported() {
        //we don't want anybody but us calling reset
        return false;
    }

    @Override
    public void setPreProcessor(MultiDataSetPreProcessor multiDataSetPreProcessor) {/*example doesn't even use the preprocessor, so let's not bother*/}

    @Override
    public MultiDataSetPreProcessor getPreProcessor() {
        /*example doesn't even use the preprocessor, so let's not bother*/
        return null;
    }
}
