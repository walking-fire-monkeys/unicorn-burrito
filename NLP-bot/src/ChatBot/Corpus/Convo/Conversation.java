package ChatBot.Corpus.Convo;

import java.util.ArrayList;
import java.util.List;

public class Conversation {
    private List<Integer> convo_line_numbers;
    private List<ConvoLine> conversation;
    private ConvoLine last_line = null;

    public Conversation ( List<Integer> convo_line_numbers) {
        this.convo_line_numbers = convo_line_numbers;
        conversation = new ArrayList<>();
    }

    public int getLineNumber(int index){
        if(index < convo_line_numbers.size()) {
            return convo_line_numbers.get(index);
        }
        return -1; //probably merged lines were in here
    }

    public List<ConvoLine> getConversation(){
        return conversation;
    }

    /*Adds new line into conversation, and links appropriately*/
    public void add(ConvoLine newLine, int line_number) throws Exception {
        if(newLine != null) {
            if (!convo_line_numbers.contains(line_number)) {
                throw new Exception("This is not the correct conversation");
            }
        } else {
            throw new NullPointerException("Null new line.. wtf dummy");
        }
        //we assume the data is sorted, meaning that inserting and merging between lines isn't a concern
        if(last_line == null) {
            last_line = newLine;
            conversation.add(newLine);
        } else if (last_line.speaker_id == newLine.speaker_id) {
            last_line.line += newLine.line;
        } else {
            newLine.prev = last_line;
            last_line.next = newLine;
            conversation.add(newLine);
            last_line = newLine;
        }
    }

    public boolean contains(int line_number){
        return convo_line_numbers.contains(line_number);
    }

    /** Returns number of examples contained within */
    public int size(){
        return conversation.size() - 1;
    }
}
