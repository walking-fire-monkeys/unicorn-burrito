package ChatBot.Corpus.Convo;

import java.util.ArrayList;
import java.util.List;

public class ConvoGroup {
    private List<Conversation> conversations = new ArrayList<>();


    public List<Conversation> getConversations(){
        return conversations;
    }

    public void addConvo(Conversation convo){
        conversations.add(convo);
    }

    public Conversation findConversation(int line_number){
        for(Conversation convo : conversations){
            if(convo.contains(line_number)){
                return convo;
            }
        }
        return null;
    }

    /** Returns number of examples contained within */
    public int size(){
        int counter = 0;
        for(Conversation convo : conversations){
            counter += convo.getConversation().size() - 1;
        }
        return counter;
    }
}
