package ChatBot.RNN;

import ChatBot.Corpus.Convo.ConvoIterator;
import ChatBot.Corpus.Corpus;
import ChatBot.Dictionary.NetworkDictionary;
import ChatBot.Main;
import com.google.common.graph.Network;
import edu.stanford.nlp.simple.Sentence;
import org.apache.commons.lang3.ArrayUtils;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.ComputationGraphConfiguration.GraphBuilder;

import org.deeplearning4j.nn.workspace.LayerWorkspaceMgr;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.RmsProp;
import org.deeplearning4j.nn.conf.inputs.InputType;

import org.deeplearning4j.nn.conf.layers.Layer;
import org.deeplearning4j.nn.conf.layers.EmbeddingLayer;
import org.deeplearning4j.nn.conf.layers.LSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;

import org.deeplearning4j.nn.conf.graph.GraphVertex;
import org.deeplearning4j.nn.conf.graph.rnn.LastTimeStepVertex;
import org.deeplearning4j.nn.conf.graph.rnn.DuplicateToTimeSeriesVertex;
import org.deeplearning4j.nn.conf.graph.MergeVertex;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class NeuralNet {
    private boolean trained = false;
    private ComputationGraph rnn_graph;
    String layer1_name = "embedding_layer";
    String layer2_name = "encoding_layer";

    String vertex1_name = "thought_vertex";
    String vertex2_name = "dupe_vertex";
    String vertex3_name = "merge_vertex";

    String layer3_name = "decoder_layer";
    String layer4_name = "output_layer";
    
    public NeuralNet(NetProperties np, LayerProperties lp){
        NeuralNetConfiguration.Builder builder = new NeuralNetConfiguration.Builder()
                .updater(new RmsProp(np.LEARNING_RATE))
                .weightInit(np.WEIGHT_INIT)
                .gradientNormalization(np.GRADIENT_NORM);

        GraphBuilder graph_builder = builder.graphBuilder()
                .backpropType(np.BACKPROP_TYPE)
                .tBPTTBackwardLength(np.BACKPROP_BACKWARD_LENGTH)
                .tBPTTForwardLength(np.BACKPROP_FORWARD_LENGTH);

        //Setting up the graph inputs
        String input1, input2;
        input1 = "source_input";
        input2 = "vertex_input";
        InputType input_type = InputType.recurrent(lp.Embedding.In);
        graph_builder
                .addInputs(input1, input2)
                .setInputTypes(input_type, input_type);

        //Start building graph
        //--prepare layers + vertices
        Layer layer1 =  new EmbeddingLayer.Builder()
                .nIn(lp.Embedding.In)
                .nOut(lp.Embedding.Out)
                .build();
        Layer layer2 = new LSTM.Builder()
                .nIn(lp.Encoding.In)
                .nOut(lp.Encoding.Out)
                .activation(lp.Encoding.FnAct)
                .build();
        GraphVertex vert1 = new LastTimeStepVertex(input1);
        GraphVertex vert2 = new DuplicateToTimeSeriesVertex(input2);
        GraphVertex vert3 = new MergeVertex();
        Layer layer3 = new LSTM.Builder()
                .nIn(lp.Decoder.In)
                .nOut(lp.Decoder.Out)
                .activation(lp.Decoder.FnAct)
                .build();
        Layer layer4 = new RnnOutputLayer.Builder()
                .nIn(lp.Output.In)
                .nOut(lp.Output.Out)
                .activation(lp.Output.FnAct)
                .lossFunction(lp.Output.FnLoss)
                .build();

        //--build graph
        graph_builder.addLayer(layer1_name, layer1, input1);
        graph_builder.addLayer(layer2_name, layer2, layer1_name);

        graph_builder.addVertex(vertex1_name, vert1, layer2_name);
        graph_builder.addVertex(vertex2_name, vert2, vertex1_name);
        graph_builder.addVertex(vertex3_name, vert3, input2, vertex2_name);

        graph_builder.addLayer(layer3_name, layer3, vertex3_name);
        graph_builder.addLayer(layer4_name, layer4, layer3_name);

        //--specifying output layers (we only have one)
        graph_builder.setOutputs(layer4_name);

        rnn_graph = new ComputationGraph(graph_builder.build());
        rnn_graph.init();
    }

    public void serial_train(Corpus corpus, NetworkDictionary dict){
        serial_train(new ConvoIterator(corpus,dict,Main.CUTOFF));
    }

    public void para_train(Corpus corpus, NetworkDictionary dict){
        para_train(new ConvoIterator(corpus,dict,Main.CUTOFF));
    }

    public void batch_train(Corpus corpus, NetworkDictionary dict){
        batch_train(new ConvoIterator(corpus,dict,Main.CUTOFF));
    }

    public void serial_train(ConvoIterator iter){
        System.out.println("Training network...");
        while(iter.hasNext()){
            rnn_graph.fit(iter.serialNext());
        }
        trained = true;
        System.out.println("Training complete.");
    }

    public void para_train(ConvoIterator iter){
        System.out.println("Training network...");
        rnn_graph.fit(iter);
        trained = true;
        System.out.println("Training complete.");
    }

    public void batch_train(ConvoIterator iter){
        System.out.println("Training network...");
        while(iter.hasNext()){
            rnn_graph.fit(iter.next());
        }
        trained = true;
        System.out.println("Training complete.");
    }

    protected double remap_range(double value, double r1_low, double r1_high, double r2_low, double r2_high) {
        return r2_low + (value - r1_low) * (r2_high - r2_low) / (r1_high - r1_low);
    }

    protected double random_range(double min, double max, Random rng){
        return remap_range(rng.nextDouble(),0,1,min,max);
    }

    public String output(NetworkDictionary dict, String input){
        final int ROW_SIZE = 100;
        String response = "";
        List<Double> prompt = dict.encodeResponse(new Sentence(input));
//        List<Double> response = null; //todo: implement response generation
//        return dict.decodeResponse(response);

        //brush teeth
        // make sure to floss as well
        // I have to admit that I like Josh's keyboard quite a lot
        // I am so incredibly lost -- Emily
        rnn_graph.rnnClearPreviousState();
        Collections.reverse(prompt);

        //prepare data.. in "formats"
        INDArray in = Nd4j.create(ArrayUtils.toPrimitive(prompt.toArray(new Double[0])), new int[] { 1, 1, prompt.size() });
        double[] decodeArr = new double[dict.vocabularySize()]; //codes of words
        decodeArr[Main.GO.intValue()] = 1; //enable <go> bit (one-hot bb)
        INDArray decode = Nd4j.create(decodeArr, new int[] { 1, dict.vocabularySize(), 1 }); // {fixed point, word size, 1 because there is no response}

        //send the data to the inputs
        rnn_graph.feedForward(new INDArray[] { in, decode }, false, false);

        //get a bunch of graph components
        org.deeplearning4j.nn.layers.recurrent.LSTM decoder = (org.deeplearning4j.nn.layers.recurrent.LSTM) rnn_graph.getLayer(layer3_name);
        org.deeplearning4j.nn.api.Layer output = rnn_graph.getLayer(layer4_name);
        org.deeplearning4j.nn.graph.vertex.GraphVertex mergeVertex = rnn_graph.getVertex(vertex3_name);
        INDArray thoughtVector = mergeVertex.getInputs()[1];

        //get a wtf-mgr..! for reasons.. related to graph components
        LayerWorkspaceMgr mgr = LayerWorkspaceMgr.noWorkspaces();
        Random rng = new Random();

        for (int row = 0; row < ROW_SIZE; ++row) {

            //important procedure
            mergeVertex.setInputs(decode, thoughtVector);
            INDArray merged = mergeVertex.doForward(false, mgr);
            INDArray activateDec = decoder.rnnTimeStep(merged, mgr);
            INDArray out = output.activate(activateDec, false, mgr);

            //we have a random number
            //double d = rnd.nextDouble(); //this is okay because the dictionary is ordered based on frequency (in this example code)
            double sum = 0.0;
            int idx = -1;
            final double mid = 0.5;
            final double step = 0.35;

            //looping over dictionary size probably
            for (int s = 0; s < out.size(1); ++s) {
                //running sum of output row up to element s
                sum += out.getDouble(0, s, 0);
                if ( random_range(mid-step,mid+step,rng) <= sum ) {
                    //set idx to the column (ie. the dictionary word)
                    idx = s;
                    break;
                }
            }
            if (idx != Main.UNK && idx != Main.EOS.intValue()) {
                response += dict.decodeWord((double) idx) + " ";
            } else if (idx == Main.EOS.intValue()) {
                break;
            } else if (idx == -1) {
                idx = Main.UNK.intValue();
            }

            //decode layer takes a single token, we tell it what word that token is... I guess..
            double[] newDecodeArr = new double[dict.vocabularySize()];
            newDecodeArr[idx] = 1;
            decode = Nd4j.create(newDecodeArr, new int[] { 1, dict.vocabularySize(), 1 });
        }
        return response;
    }

    /** Serializes a trained network and saves it the file specified. */
    public void serialSave(File outFile){
        if(trained){
            System.out.println("Saving network model to file " + outFile.getAbsolutePath());
            try {
                File backup = new File(outFile.getAbsolutePath() + ".bak");
                if(outFile.exists()){
                    if(backup.exists()){
                        backup.delete();
                    }
                    outFile.renameTo(backup);
                }
                ModelSerializer.writeModel(rnn_graph,outFile,true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /** Loads a trained serialized network, returns true if successful. */
    public boolean serialLoad(File inFile){
        if(inFile.exists()){
            System.out.println("Loading network model from file " + inFile.getAbsolutePath());
            try {
                rnn_graph = ModelSerializer.restoreComputationGraph(inFile);
                trained = true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            return true;
        }
        return false;
    }
}
