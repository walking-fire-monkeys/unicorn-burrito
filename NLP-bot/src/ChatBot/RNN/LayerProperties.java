package ChatBot.RNN;


import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public class LayerProperties {
    public class LayerProp{
        public int In;
        public int Out;
        public Activation FnAct = Activation.TANH;
        public LossFunctions.LossFunction FnLoss = LossFunctions.LossFunction.SQUARED_LOSS;
    }
    public LayerProp Embedding = new LayerProp();
    public LayerProp Encoding = new LayerProp();
    public LayerProp Decoder = new LayerProp();
    public LayerProp Output = new LayerProp();
    public LayerProperties(int encoding_size, int embedding_width, int hidden_layer_width, int decoding_size){
        //todo: double check validity of setup
        Embedding.In = encoding_size;
        Embedding.Out = embedding_width;

        Encoding.In = embedding_width;
        Encoding.Out = hidden_layer_width;

        Decoder.In = decoding_size + hidden_layer_width;
        Decoder.Out = hidden_layer_width;

        Output.In = hidden_layer_width;
        Output.Out = decoding_size;
    }
}
