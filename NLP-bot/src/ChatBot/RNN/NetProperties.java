package ChatBot.RNN;

import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.weights.WeightInit;

public class NetProperties {
    public double LEARNING_RATE;
    public WeightInit WEIGHT_INIT;
    public GradientNormalization GRADIENT_NORM;
    public BackpropType BACKPROP_TYPE;
    public int BACKPROP_BACKWARD_LENGTH;
    public int BACKPROP_FORWARD_LENGTH;

    public NetProperties(double Learn_Rate, WeightInit Weight_Init, GradientNormalization Gradient_Norm, BackpropType BP_Type, int BPFL, int BPBL) throws Exception {
        if(BPFL < BPBL){
            throw new Exception("BPTT Forward Length must be >= BPTT Backward Length");
        }
        LEARNING_RATE = Learn_Rate;
        WEIGHT_INIT = Weight_Init;
        GRADIENT_NORM = Gradient_Norm;
        BACKPROP_TYPE = BP_Type;
        BACKPROP_FORWARD_LENGTH = BPFL;
        BACKPROP_BACKWARD_LENGTH = BPBL;
    }
}
