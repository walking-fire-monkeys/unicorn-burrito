package ChatBot.Dictionary;

import ChatBot.Main;
import edu.stanford.nlp.simple.Sentence;
import edu.stanford.nlp.semgraph.*;

import javax.sql.rowset.serial.SerialArray;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;
import java.io.*;

/**
 * NetworkDictionary maps encoding data, and provides an interface to encode prompts and responses.
 *
 * Note: Class should be redesigned later on to decouple encoding from the mapping data structure.
 * As well as to decouple prompts from responses with regards to encoding/decoding.
 *
 * todo: create code for enabling or disabling sub-dictionaries
 * */
public class NetworkDictionary {
    private double index = 0;
    private Map<String, Integer> frequency = new HashMap<>();
    private Map<String, Double> vocabulary = new HashMap<>();
    private Map<Double, String> vocabulary_lookup = new HashMap<>();

    private Map<String, Double> lemma = new HashMap<>();
    private Map<String, Double> NER = new HashMap<>();
    private Map<String, Double> POS = new HashMap<>();
    private Map<Double, Double> lemmaRelations = new HashMap<>();
    private Map<Double, Double> nerRelations = new HashMap<>();
    private Map<Double, Double> posRelations = new HashMap<>();

    /** Deletes dictionary data. */
    public void clear(){
        frequency.clear();
        vocabulary.clear();
        vocabulary_lookup.clear();
        lemma.clear();
        NER.clear();
        POS.clear();
    }

    /** Parses sentence, maps parsed data into dictionary. */
    public void mapSentence(Sentence sentence) {
        if(frequency == null){
            frequency = new HashMap<>();
        }
        ArrayList<String> rawData = new ArrayList<>(sentence.words());
        ArrayList<String> lemmaVals = new ArrayList<>(sentence.lemmas());
        ArrayList<String> nerVals = new ArrayList<>(sentence.nerTags());
        ArrayList<String> posVals = new ArrayList<>(sentence.posTags());

        // Adds the words and punctuation characters to maps.
        for (int i = 0; i < rawData.size(); i++) {
            String word = rawData.get(i);
            if(nerVals.get(i).compareTo("O") == 0){
                word = word.toLowerCase();
            }
            if (!vocabulary.containsKey(word)) {
                vocabulary.put(word, index);
                vocabulary_lookup.put(index++, word);
                // increases frequency for each word/special character
                frequency.put(word, 1);
            } else {
                frequency.replace(word, frequency.get(word) + 1);
            }

            /*double vocab_encoding = vocabulary.get(word);
            if (!lemma.containsKey(lemmaVals.get(i))) {
                lemma.put(lemmaVals.get(i), index++);
            }
            if (!NER.containsKey(nerVals.get(i))) {
                NER.put(nerVals.get(i), index++);
            }
            if (!POS.containsKey(posVals.get(i))) {
                POS.put(posVals.get(i), index++);
            }
            lemmaRelations.put(vocab_encoding,lemma.get(lemmaVals.get(i)));
            nerRelations.put(vocab_encoding,NER.get(nerVals.get(i)));
            posRelations.put(vocab_encoding, POS.get(posVals.get(i)));*/
        }
    }

    /** Tokenizes a prompt sentence, and encodes each token with multiple values */
    public List<List<Double>> encodePrompt(Sentence sentence) {
        // Finds the values of all the attributes of each word: raw data, lemma, ner, pos
        ArrayList<String> rawWords = new ArrayList<>(sentence.words());
        ArrayList<String> nerVals = new ArrayList<>(sentence.nerTags());
        List<List<Double>> outer = new ArrayList<>();

        // Need to make each list of doubles before inserting them into the outer list.
        for (int i = 0; i < rawWords.size(); i++) {
            //todo: implement dependencies, or not.
            String word = rawWords.get(i);

            if(nerVals.get(i).compareTo("O") == 0) {
                word = word.toLowerCase();
            }

            List<Double> inner = new ArrayList<>();
            Double encoded_word = vocabulary.get(word);
            if(encoded_word == null){
                encoded_word = Main.UNK;
            }
            inner.add(encoded_word);
            inner.add(lemmaRelations.get(encoded_word));
            inner.add(nerRelations.get(encoded_word));
            inner.add(posRelations.get(encoded_word));
            outer.add(inner);
        }
        return outer;
    }

    /** Tokenizes a response sentence, and encodes each token with a single value */
    public List<Double> encodeResponse(Sentence sentence){
        List<Double> response = new ArrayList<>(sentence.words().size());
        int i = 0;
        for(String word : sentence.words()){
            Double encoded_word;
            if(sentence.nerTag(i++).compareTo("O") == 0) {
                encoded_word = vocabulary.get(word.toLowerCase());
            } else {
                encoded_word = vocabulary.get(word);
            }

            if(encoded_word != null){
                response.add(encoded_word);
            } else {
                response.add(Main.UNK);
            }
        }
        return response;
    }

    /** Decodes a single-digit-encoded word. */
    public String decodeWord(double encoded_word) {
        return vocabulary_lookup.get(encoded_word);
    }

    /** Decodes a sequence of single-digit-encoded words. */
    public String decodeResponse(List<Double> encoded_words) {
        ArrayList<String> decodedWords = new ArrayList<>();
        ArrayList<String> specialCharacters = new ArrayList<>();
        specialCharacters.add(",");
        specialCharacters.add(".");
        specialCharacters.add("$");
        specialCharacters.add("?");
        specialCharacters.add("!");
        specialCharacters.add("#");
        specialCharacters.add("%");
        specialCharacters.add("&");
        specialCharacters.add("[");
        specialCharacters.add("]");
        specialCharacters.add("{");
        specialCharacters.add("}");
        specialCharacters.add("-");
        specialCharacters.add("_");
        specialCharacters.add(":");
        specialCharacters.add(";");
        for (Double word : encoded_words){
            decodedWords.add(decodeWord(word));
        }
        String decodedSentence = "";
        decodedSentence = decodedSentence + decodedWords.get(0);
        for (int i = 1; i < decodedWords.size(); i++) {
            if (specialCharacters.contains(decodedWords.get(i))){
                decodedSentence = decodedSentence + decodedWords.get(i);
            }
            else{
                decodedSentence = decodedSentence + " " + decodedWords.get(i);
            }

        }
        return decodedSentence;
    }

    private List<Double> getParents(String word, Sentence sentence){
      /*//  IndexedWord index = new IndexedWord(word);
        SemanticGraphFactory.Mode mode;
        mode = SemanticGraphFactory.Mode.BASIC;
        SemanticGraph dep = sentence.dependencyGraph(mode);
       // dep.
      //  dep.getParents(word).toString()*/
        return null;
    }

    /** Serializes dictionary data, saves to file(s). */
    public void serialSave(File outFile) {
        System.out.println("Saving dictionary to " + outFile.getAbsolutePath());
        try {
            ObjectOutputStream ObjOut = new ObjectOutputStream(new FileOutputStream(outFile));

            ObjOut.writeObject(frequency);
            ObjOut.writeObject(vocabulary);
            ObjOut.writeObject(vocabulary_lookup);
            ObjOut.writeObject(lemma);
            ObjOut.writeObject(NER);
            ObjOut.writeObject(POS);
            ObjOut.writeObject(lemmaRelations);
            ObjOut.writeObject(nerRelations);
            ObjOut.writeObject(posRelations);

            ObjOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /** Loads serialized dictionary data. */
    @SuppressWarnings("unchecked")
    public void serialLoad(File inFile) {
        System.out.println("Loading dictionary from " + inFile.getAbsolutePath());
        try {
            ObjectInputStream inputObj = new ObjectInputStream(new FileInputStream(inFile));

            frequency = (Map) inputObj.readObject();
            vocabulary = (Map) inputObj.readObject();
            vocabulary_lookup = (Map) inputObj.readObject();
            lemma = (Map) inputObj.readObject();
            NER = (Map) inputObj.readObject();
            POS = (Map) inputObj.readObject();
            lemmaRelations = (Map) inputObj.readObject();
            nerRelations = (Map) inputObj.readObject();
            posRelations = (Map) inputObj.readObject();

            inputObj.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* Getters */
    public int vocabularySize(){
        return vocabulary.size();
    }

    public int encodingSize(){
        return (vocabulary.size() + lemma.size() + NER.size() + POS.size());
    }
    public Map<String, Integer> getFrequency() { return frequency; }
    public Map<String, Double> getVocab() { return vocabulary; }
    public Map<Double, String> getVocabLookup() { return vocabulary_lookup; }
    public Map<String, Double> getLemma() { return lemma; }
    public Map<String, Double> getNER() { return NER; }
    public Map<String, Double> getPOS() { return POS; }
    public Map<Double, Double> getNerRelations (){
        return nerRelations;
    }

}
