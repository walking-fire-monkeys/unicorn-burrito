package ChatBot;

import ChatBot.Corpus.Convo.Conversation;
import ChatBot.Corpus.Convo.ConvoGroup;
import ChatBot.Corpus.Convo.ConvoLine;
import ChatBot.Corpus.Corpus;
import ChatBot.Dictionary.NetworkDictionary;
import ChatBot.RNN.LayerProperties;
import ChatBot.RNN.NetProperties;
import ChatBot.RNN.NeuralNet;
import org.deeplearning4j.nn.conf.BackpropType;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.weights.WeightInit;
import edu.stanford.nlp.simple.Sentence;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.nativeblas.NativeOps;
import org.nd4j.nativeblas.NativeOpsHolder;
import org.nd4j.nativeblas.Nd4jBlas;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/* ChatBot Unicorn Burrito
***************************
* This project is a chat bot.
*
* Author s:  Emily Earl, Josh Cooper, Anthony Boyko,         Michael Woods
* Date:          2019-01-27
* Last Modified: 2019-02-14
*/
public class Main {
    private Corpus corp = new Corpus();
    private NeuralNet net = null;
    private NetworkDictionary dict = new NetworkDictionary();
    private boolean training_mode = false;
    private boolean load_model = false;
    //default files
    private String model_filename = "default_model.dat";
    private String dictionary_filename = "default_dict.dat";
    public static Double UNK;
    public static Double EOS;
    public static Double GO;
    public static int CUTOFF = 14;

    public static void main(String[] args){
        Main m = new Main();
        //parse command line arguments for file names
        for(int i = 0; i < args.length; i += 2) {
            if(args[i].contains("retrain")) {
                m.load_model = true;
                m.training_mode = true;
                m.model_filename = "\\" + args[i+1];
                m.dictionary_filename = "\\dictionary-" + args[i+1];
            } else if(args[i].contains("train")) {
                m.load_model = false;
                m.training_mode = true;
                m.model_filename = "\\" + args[i+1];
                m.dictionary_filename = "\\dictionary-" + args[i+1];
            } else if (args[i].contains("load")) {
                m.load_model = true;
                m.training_mode = false;
                m.model_filename = "\\" + args[i+1];
                m.dictionary_filename = "\\dictionary-" + args[i+1];
            }
        }
        m.run();
    }

    private void run(){
        try {
            init();
            run_loop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {
        final Path MODELSAVE_PATH = FileSystems.getDefault().getPath("Model");
        File model_file = new File(MODELSAVE_PATH + "\\" + model_filename);
        File dictionary_file = new File(MODELSAVE_PATH + "\\" + dictionary_filename);

        Nd4jBlas nd4jBlas = (Nd4jBlas) Nd4j.factory().blas();

        System.out.println("Loading data set. Building Corpus..");
        corp.loadCorpus();
        System.out.println(corp.toString()); //print corpus info
//        System.out.println("Building sentences..");
//        corp.buildSentences();
//        System.out.println("Done building sentences.");

        boolean dictionary_exists = false;
        if(load_model){
            if(dictionary_file.exists()) {
                dict.serialLoad(dictionary_file);
                Sentence special_tags_sent = new Sentence("<unk> <eos> <go>");
                List<Double> special_tags = dict.encodeResponse(special_tags_sent);
                UNK = special_tags.get(0);
                EOS = special_tags.get(1);
                GO = special_tags.get(2);
                System.out.println("Dictionary loaded!!\nVocabulary mappings: " + dict.vocabularySize());
            } else {
                throw new Exception("Cannot load model, corresponding dictionary does not exist.");
            }
        } else {
            build_dict();
            System.out.println("The dictionary is built! Huzzah!\nVocabulary mappings: " + dict.vocabularySize());
            dictionary_file.getParentFile().mkdirs();
            dict.serialSave(dictionary_file);
        }

        NetProperties net_prop = new NetProperties(0.0009,WeightInit.XAVIER,GradientNormalization.RenormalizeL2PerLayer,BackpropType.Standard,25,25);
        final int columns = dict.vocabularySize();
        LayerProperties layer_prop = new LayerProperties(columns,columns/10,columns/10,columns);
        layer_prop.Encoding.FnAct = Activation.TANH;
        layer_prop.Decoder.FnAct = Activation.TANH;
        layer_prop.Output.FnAct = Activation.SOFTMAX;
        layer_prop.Output.FnLoss = LossFunctions.LossFunction.MCXENT;
        net = new NeuralNet(net_prop, layer_prop);
        if(load_model){
            if(model_file.exists()) {
                net.serialLoad(model_file);
            } else {
                training_mode = true;
            }
        }
        if(training_mode){
            for(int i = 0; i < 1200; ++i) {
                net.para_train(corp, dict);
            }
            net.serialSave(model_file);
        }
    }

    /**Loops for as long as the user wishes*/
    private void run_loop(){
        System.out.println("Type /quit to exit.");
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.print("You: ");
            String input = in.nextLine();
            if(input.contains("/quit")){
                return;
            }
            System.out.println("Bot: " + net.output(dict,input));
        }
    }

    private void build_dict(){
        System.out.println("Building dictionary");
        int counter = 0;
        Sentence special_tags_sent = new Sentence("<unk> <eos> <go>");
        dict.mapSentence(special_tags_sent);
        List<Double> special_tags = dict.encodeResponse(special_tags_sent);
        UNK = special_tags.get(0);
        EOS = special_tags.get(1);
        GO = special_tags.get(2);

        for(Map.Entry<Integer, ConvoGroup> entry : corp.getDataSet().entrySet()){
            for(Conversation convo : entry.getValue().getConversations()){
                for(ConvoLine line : convo.getConversation()){
                    if(++counter % 100 == 0){
                        System.out.println("lines processed: " + counter);
                    }
                    if(counter >= Main.CUTOFF ){
                        return;
                    }
                    if(line.sent == null){
                        line.sent = new Sentence(line.line);
                    }
                    dict.mapSentence(line.sent);
                }
            }
        }

    }
}
