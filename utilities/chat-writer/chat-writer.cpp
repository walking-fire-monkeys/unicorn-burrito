#include "stdafx.h"
#include "chat-writer.h"
//#include <cstdio>


template<typename ... Args>
std::string string_format( const std::string& format, Args ... args ){
    size_t size = snprintf( nullptr, 0, format.c_str(), args ... ) + 1; // Extra space for '\0'
    std::unique_ptr<char[]> buf( new char[ size ] ); 
    snprintf( buf.get(), size, format.c_str(), args ... );
    return std::string( buf.get(), buf.get() + size - 1 ); // We don't want the '\0' inside
}

ChatWriter::ChatWriter(std::string p, std::string prefix){
    path = fs::absolute(fs::path(p));
    if(!fs::exists(path)){
        fs::create_directories(path);
    }
    fs::path convo_path = fs::absolute(fs::path(p).append(prefix + "-conversations.txt"));
    fs::path dialogue_path = fs::absolute(fs::path(p).append(prefix + "-dialogue.txt"));
    if(fs::exists(dialogue_path)){
        read_mode = true;
        dialogue.open(dialogue_path,std::ios::in);
        line_number = find_highest_line_number();
        dialogue.close();
    }
    read_mode = false;
    auto mode = std::ios::out | std::ios::app;
    convo.open(convo_path,mode);
    dialogue.open(dialogue_path,mode);
    if(convo.fail()){
        printf("Error opening convo file: %s\n",convo_path.string().c_str());
    }
    if(dialogue.fail()){
        printf("Error opening dialogue file: %s\n",dialogue_path.string().c_str());
    }
    srand(time(NULL));
    gid = rand();
}

void ChatWriter::new_convo(){
    if(conversation_lines.size() < 2){
        printf("  ERROR: Cannot start a new conversation while the current has less than two lines");
        return;
    }
    std::string line = string_format("u0 - u1 - g%d +++$+++ [", gid);
    bool first = true;
    for(auto line_num : conversation_lines){
        if(first){
            line += string_format("'%d'", line_num);
            first = false;
        } else {
            line += string_format(", '%d'", line_num);
        }
    }
    conversation_lines.clear();
    line += "]\n";
    convo.write(line.c_str(),line.size());
}

void ChatWriter::close(){
    if(!conversation_lines.empty()){
        new_convo();
    }
    convo.close();
    dialogue.close();
}

int ChatWriter::alternating_number(){
    static bool alternate = true;
    alternate = !alternate;
    if(alternate){
        return 1;
    } else {
        return 0;
    }
}

int ChatWriter::find_highest_line_number(){
    try{
        std::regex pattern("L([0-9]+)");
        std::string line;
        int highest = 0;
        if(read_mode){
            while(std::getline(dialogue,line)){
                std::smatch match;
                if(std::regex_search(line,match,pattern)){
                    for(auto m : match){
                        std::stringstream number(m.str());
                        int cur_number;
                        number >> cur_number;
                        if(!number.fail()){
                            if(cur_number > highest){
                                highest = cur_number;
                            }
                        }
                    }
                }
            }
            return highest;
        }
        return -1;
    } catch(std::exception e){
        printf("exception: %s",e.what());
        exit(1);
    }
}

void ChatWriter::build_dialogue(){
    std::string line;
    int uid = 0;
    printf("Begin writing dialogue.\n");
    while(true){
        uid = alternating_number();
        printf(uid == 0 ? "" : ">");
        std::getline(std::cin,line);
        if(line == "/quit"){
            break;
        }else if(line == "/new"){
            new_convo();
            printf("Starting new conversation..\n\n");
            continue;
        }else if(line == "/help"){
            print_help();
            alternating_number();
            printf("coming soon..\n\n");
            continue;
        }
        line = string_format("L%d - u%d - g%d +++$+++ %s\n",++line_number,uid,gid,line.c_str());
        dialogue.write(line.c_str(),line.size());
        conversation_lines.push_back(line_number);
    }
    close();
}

