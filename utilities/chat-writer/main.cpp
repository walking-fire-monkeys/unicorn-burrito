#include "stdafx.h"
#include "chat-writer.h"
#include "input.h"

std::string get_fileprefix();
std::string get_path();


int main(int argc,char *argv[]){
    //std::ios_base::sync_with_stdio(false); //synced by default
    std::ios_base::sync_with_stdio(true);
    fs::path convo;
    std::string prefix;
    std::string path;
    std::cout << "Conversational DataSet Builder 0.1\n==========================" << std::endl;
    path = get_path();
    prefix = get_fileprefix();
    ChatWriter writer(path,prefix);
    writer.build_dialogue();
    return 0;
}

std::string get_path(){
    std::string path;
    bool repeat = false;
    do{
        do{
            if(repeat){
                std::cout << " path cannot contain spaces\n";
            }
            std::cout << "Enter the file path to save in: ";
            std::getline(std::cin,path);
            repeat = true;
        } while(path.find(" ") != std::string::npos);
        repeat = false;
        fs::path dest = fs::absolute(fs::path(path));
        printf("The path you entered is: %s\nIs this correct? ",dest.string().c_str());
    } while(!verify());
    return path;
}

std::string get_fileprefix(){
    std::string fileprefix;
    bool repeat = false;
    do{
        if(repeat){
            std::cout << " input cannot contain spaces\n";
        }
        std::cout << "Enter a file name prefix: ";
        std::getline(std::cin,fileprefix);
        repeat = true;
    } while(fileprefix.find(" ") != std::string::npos);
    return fileprefix;
}