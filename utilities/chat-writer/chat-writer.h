#pragma once
#include "stdafx.h"

class ChatWriter
{
private:
    //std::fstream
    int line_number = 0;
    int gid = 0;
    bool read_mode = true;
    fs::path path;
    std::fstream convo;
    std::fstream dialogue;
    std::vector<int> conversation_lines;
protected:
    void new_convo();
    void close();
    int alternating_number();
    int find_highest_line_number();
public:
    ChatWriter(std::string path, std::string prefix);
    void print_help(){};
    void build_dialogue();
};

