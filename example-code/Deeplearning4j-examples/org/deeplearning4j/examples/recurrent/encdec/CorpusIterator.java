package org.deeplearning4j.examples.recurrent.encdec;

import org.apache.commons.lang3.ArrayUtils;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.MultiDataSet;
import org.nd4j.linalg.dataset.api.MultiDataSetPreProcessor;
import org.nd4j.linalg.dataset.api.iterator.MultiDataSetIterator;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.INDArrayIndex;
import org.nd4j.linalg.indexing.NDArrayIndex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("serial")
public class CorpusIterator implements MultiDataSetIterator {

    /*
     * Motivation: I want to get asynchronous data iteration while not blocking on net.fit() until the end of epoch. I want to checkpoint
     * the network, show intermediate test results and some stats, it would be harder to achieve with listeners I think so this is how I
     * solved the problem. This way the learn process is asynchronous inside one macrobatch and synchronous across all the macrobatches.
     *
     * Macrobatch is a group of minibatches. The iterator is modified so that it reports the end of data when it exhausts a macrobatch. Then
     * it advances (manually) to the next macrobatch.
     */

    private List<List<Double>> corpus;
    private int batchSize;
    private int batchesPerMacrobatch;
    private int totalBatches;
    private int totalMacroBatches;
    private int currentBatch = 0;
    private int currentMacroBatch = 0;
    private int dictSize;
    private int rowSize;
    private MultiDataSetPreProcessor preProcessor;

    public CorpusIterator(List<List<Double>> corpus, int batchSize, int batchesPerMacrobatch, int dictSize, int rowSize) {
        this.corpus = corpus;
        this.batchSize = batchSize;
        this.batchesPerMacrobatch = batchesPerMacrobatch;
        this.dictSize = dictSize;
        this.rowSize = rowSize;
        totalBatches = (int) Math.ceil((double) corpus.size() / batchSize);
        totalMacroBatches = (int) Math.ceil((double) totalBatches / batchesPerMacrobatch);
    }

    @Override
    public boolean hasNext() {
        return currentBatch < totalBatches && getMacroBatchByCurrentBatch() == currentMacroBatch;
    }

    private int getMacroBatchByCurrentBatch() {
        return currentBatch / batchesPerMacrobatch;
    }

    @Override
    public MultiDataSet next() {
        return next(batchSize);
    }

    @Override
    public MultiDataSet next(int num) {
        int i = currentBatch * batchSize;
        int currentBatchSize = Math.min(batchSize, corpus.size() - i - 1);

        //inputs
        INDArray input = Nd4j.zeros(currentBatchSize, 1, rowSize);
        INDArray decode = Nd4j.zeros(currentBatchSize, dictSize, rowSize);

        //output prediction
        INDArray prediction = Nd4j.zeros(currentBatchSize, dictSize, rowSize);

        //masks - what are they good for? dunno yet
        INDArray inputMask = Nd4j.zeros(currentBatchSize, rowSize);
        INDArray pred_decode_Mask = Nd4j.zeros(currentBatchSize, rowSize);

        for (int j = 0; j < currentBatchSize; j++) {
            //getting encoded sentences. !These do not always represent a valid conversation dialogue though.
            List<Double> prompt = new ArrayList<>(corpus.get(i));
            List<Double> response = new ArrayList<>(corpus.get(i + 1));
            Collections.reverse(prompt);
            response.add(1.0); //add <eos>

            /**
             * Using the one-hot vectors here is merely to ensure the model categorizes words correctly
             * the alternative would be to group many words under a single variable output, and then try to convert that number to a word
             * Instead we want to convert a column to a word, or vice versa
             *
             * This may mean our dictionary assumptions were wrong
             * */

            /**
             * Matrix format
             *  - So we set the column width (ie. row length) to the dictionary size.
             *  - We also set the row height (ie. column length) to the sentence size.
             *
             * Columns:   dictionary word (ie. element 381 is the word mapped to 381 in the dictionary)
             * Rows:      token position (ie. element 0 is the first token of the sentence)
             * Accessing: matrix[column][row]
             *
             * "The fox are red red"
             *   The | fox | are | red
             * -------------------------
             * |  1  |  0  |  0  |  0  |
             * |  0  |  1  |  0  |  0  |
             * |  0  |  0  |  1  |  0  |
             * |  0  |  0  |  0  |  1  |
             * |  0  |  0  |  0  |  1  |
             * -------------------------
             * */
            double predOneHot[][] = new double[dictSize][response.size()];
            double decodeOneHot[][] = new double[dictSize][response.size()];
            decodeOneHot[2][0] = 1; // <go> token
            int predIdx = 0;
            //building the matrices, formatted as above
            for (Double pred : response) {
                predOneHot[pred.intValue()][predIdx] = 1;
                if (predIdx < response.size() - 1) {
                    decodeOneHot[pred.intValue()][predIdx + 1] = 1;
                }
                ++predIdx;
            }


            INDArrayIndex batch_plane = NDArrayIndex.point(j);
            INDArrayIndex zero = NDArrayIndex.point(0);
            INDArrayIndex prompt_size = NDArrayIndex.interval(0,prompt.size());
            INDArrayIndex response_size = NDArrayIndex.interval(0,response.size());
            INDArrayIndex encoding_size = NDArrayIndex.interval(0,dictSize);

            /**
             * INDArray.put(INDArray indices, INDArray element)
             * Put element in to the indices denoted by the indices ndarray.
             * */
            //inputs                            // Nd4j.create() - Creates a row vector with data given
            input.put(new INDArrayIndex[] {batch_plane,zero,prompt_size},
                    Nd4j.create(ArrayUtils.toPrimitive(prompt.toArray(new Double[0]))));
            decode.put(new INDArrayIndex[] {batch_plane,encoding_size,response_size}, Nd4j.create(decodeOneHot));

            //output prediction
            prediction.put(new INDArrayIndex[] {batch_plane,encoding_size,response_size}, Nd4j.create(predOneHot));

            //masks - what are they good for? dunno yet
            inputMask.put(new INDArrayIndex[] {batch_plane,prompt_size}, Nd4j.ones(prompt.size()));
            pred_decode_Mask.put(new INDArrayIndex[] {batch_plane,response_size}, Nd4j.ones(response.size()));
            ++i;
        }
        ++currentBatch;
        return new org.nd4j.linalg.dataset.MultiDataSet(
                new INDArray[] { input, decode },
                new INDArray[] { prediction },
                new INDArray[] { inputMask, pred_decode_Mask },
                new INDArray[] { pred_decode_Mask });
    }

    @Override
    public MultiDataSetPreProcessor getPreProcessor() {
        return null;
    }

    @Override
    public boolean resetSupported() {
        // we don't want this iterator to be reset on each macrobatch pseudo-epoch
        return false;
    }

    @Override
    public boolean asyncSupported() {
        return true;
    }

    @Override
    public void reset() {
        // but we still can do it manually before the epoch starts
        currentBatch = 0;
        currentMacroBatch = 0;
    }

    public int batch() {
        return currentBatch;
    }

    public int totalBatches() {
        return totalBatches;
    }

    public void setCurrentBatch(int currentBatch) {
        this.currentBatch = currentBatch;
        currentMacroBatch = getMacroBatchByCurrentBatch();
    }

    public boolean hasNextMacrobatch() {
        return getMacroBatchByCurrentBatch() < totalMacroBatches && currentMacroBatch < totalMacroBatches;
    }

    public void nextMacroBatch() {
        ++currentMacroBatch;
    }

    public void setPreProcessor(MultiDataSetPreProcessor preProcessor) {
        this.preProcessor = preProcessor;
    }
}
